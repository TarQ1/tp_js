-- CreateTable
CREATE TABLE "Plants" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "secondName" TEXT NOT NULL,
    "imageURL" TEXT NOT NULL,

    CONSTRAINT "Plants_pkey" PRIMARY KEY ("id")
);
