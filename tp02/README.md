# TP02 - Sylvain Ecuer

## Dépendances

* Docker (https://www.docker.com/)
* NodeJs (https://nodejs.org/en/)
* NPM (https://www.npmjs.com/)

## Database

###  Comment lancer et mettre en place la base de données :
- Lancer la base de données avec la commande : `sudo docker-compose up`
- Appliquer les migrations Prisma : `npx prisma migrate dev`
- Remplir la DB avec le script load_db.js : `node load_db.js ${CHEMIN_VERS_LE_FICHIER_DES_PLANTES}`
  
### Comment lancer l'appli : 
- Lancer le serveur avec la commande : `npm start`
- Allez sur votre navigateur à l'adresse : `http://localhost:3000/`
- Trouver la documentation sur : `http://localhost:3000/docs`

###  Comment lancer les tests :
- Lancer les tests avec la commande : `npm test`

### L'API :

L'api plante comporte trois routes :
- `GET /` : Renvoie la liste de toutes les plantes, avec limit et offset en query parameters
- `GET /plant/search/{prefix}` : Renvoie la liste des plantes dont le nom commence par le préfixe passé en paramètre avec limit et offset en query parameters
- `GET /plant/{id}` : Renvoie la plante dont l'id est passé en paramètre
  
L'endpoints `GET /plant/{id}` renvoie une structure de la forme suivante 
```
{
    "id": number
    "name": string,
    "secondName": string,
    "imageURL": string
}
```

Les autres endpoints plantes renvoient le même type mais en tableau.

L'api User comporte deux routes :
- `POST /user` : Créer un utilisateur avec les informations passées en body
- `PUT /user` : Met à jour l'utilisateur avec les informations passées en body

Il est nécessaire d'avoir un champ `token` dans le header de la requête pour accéder aux endpoints de l'api User. Ce token est l'user id de l'utilisateur ayant comme role `'admin'`.

Pour tester cela nous avons l'endpoint : `GET /content` qui renvoie des informations sur dynamiques en fonction de l'utilisateur connecté.