import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { PrismaService } from './../prisma.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
    constructor(private prisma: PrismaService) { }

    async use(req: Request, res: Response, next: NextFunction) {
        let auth_header = req.headers.authorization.split(' ')[1];

        console.log("Middleware executed")

        if (auth_header) {
            let userID: string = Buffer.from(auth_header, 'base64').toString('ascii');
            let user_id_int: number = parseInt(userID)
            if (!user_id_int || !user_id_int) {
                return res.status(401).send('Unauthorized');
            }
            let user = await this.prisma.users.findUnique({
                where: {
                    id: user_id_int,
                },
            })
            if (user.role === 'admin') {
                req.headers.user_id = userID;
                return next();
            }
        }
        return res.status(401).send('Unauthorized');
    }
}
