import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber } from 'class-validator';

export class UpdateUserDto {
    @IsString()
    @ApiProperty()
    role: string;

    @IsNumber()
    @ApiProperty()
    id: number;
}

