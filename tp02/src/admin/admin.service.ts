import { Injectable } from '@nestjs/common';
import { PrismaService } from './../prisma.service';
import { CreateUserDto } from './CreateUserDto';
import { UpdateUserDto } from './UpdateUserDto';

@Injectable()
export class AdminService {
  // inject the prisma service
  constructor(private prisma: PrismaService) { }

  createUser(createUserDto: CreateUserDto): any {
    console.log("Created user")

    return this.prisma.users.create({
      data: {
        role: createUserDto.role,
        age: createUserDto.age,
      },
    });
  }


  // UpdateUserDto
  updateUser(updateUserDto: UpdateUserDto): any {
    console.log("Updated user")

    return this.prisma.users.update({
      where: {
        id: updateUserDto.id,
      },
      data: {
        role: updateUserDto.role,
      },
    });
  }
}
