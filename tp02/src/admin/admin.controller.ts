import { Body, Controller, DefaultValuePipe, Get, Param, Post, Put, Query, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AdminService } from './admin.service';
import { CreateUserDto } from './CreateUserDto';
import { UpdateUserDto } from './UpdateUserDto';

@Controller('/admin')
@ApiTags('Admin')
export class AdminController {
  constructor(private readonly AdminService: AdminService) { }

  @Post('/user')
  @ApiBody({ description: 'Create a user with a role and age', type: CreateUserDto })
  @ApiBearerAuth()
  createUser(@Body(ValidationPipe) createUserDto: CreateUserDto): any {
    return this.AdminService.createUser(createUserDto);
  }

  @Put('/user')
  @ApiBody({ description: 'Update a user role', type: UpdateUserDto })
  @ApiBearerAuth()
  updateUser(@Body(ValidationPipe) createUserDto: UpdateUserDto): any {
    return this.AdminService.updateUser(createUserDto);
  }
}

