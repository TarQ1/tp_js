import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNumber } from 'class-validator';

export class CreateUserDto {
    @ApiProperty()
    @IsString()
    role: string;

    @IsNumber()
    @ApiProperty()
    age: number;
}

