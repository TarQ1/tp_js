import { Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';

@Injectable()
export class AppService {
  constructor(private prisma: PrismaService) { }

  async getAllPlants(limit: string = '10', offset: string = '0'): Promise<any> {

    let limitInt: number = parseInt(limit);
    if (!limitInt || limitInt <= 0) {
      limitInt = 10;
    }

    let offsetInt: number = parseInt(offset);
    if (!offsetInt || offsetInt <= 0) {
      offsetInt = 0;
    }

    console.log(`get all plants limit: ${limitInt} offset: ${offsetInt}`);

    return await this.prisma.plants.findMany({
      take: limitInt,
      skip: offsetInt,
      where: {
        status: true,
      }
    });
  }

  async getPlantById(id: string): Promise<any> {

    console.log(`get plant by id: ${id}`);

    let res = await this.prisma.plants.findUnique({
      where: {
        id: parseInt(id),
      },
    });
    if (!res.status) {
      return null;
    }

  }

  async getPlantByPrefix(prefix: string, limit: string = '10', offset: string = '0'): Promise<any> {
    if (!prefix) {
      prefix = "";
    }
    let limitInt: number = parseInt(limit);
    if (!limitInt || limitInt <= 0) {
      limitInt = 10;
    }
    let offsetInt: number = parseInt(offset);
    if (!offsetInt || offsetInt <= 0) {
      offsetInt = 0;
    }

    console.log(`get plant by prefix: ${prefix} limit: ${limitInt} offset: ${offsetInt}`);

    return await this.prisma.plants.findMany({
      where: {
        name: {
          startsWith: prefix,
          mode: "insensitive",
        },
        status: true,
      },
      take: limitInt,
      skip: offsetInt,
    });
  }

  async getContent(user_id: number): Promise<any> {
    if (user_id) {
      let user = await this.prisma.users.findUnique({
        where: {
          id: user_id,
        },
      });
      if (user) {
        return user.age
      }
    }
    return 0;
  }
}
