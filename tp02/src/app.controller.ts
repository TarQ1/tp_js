import { Controller, DefaultValuePipe, Get, Param, Query, Req } from '@nestjs/common';
import { ApiBearerAuth, ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { AppService } from './app.service';

@Controller()
@ApiTags('Plants')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @ApiQuery({ name: "limit", required: false })
  @ApiQuery({ name: "offset", required: false })
  @Get('/')
  getAllPlants(@Query('limit') limit: string, @Query('offset') offset: string): any {
    return this.appService.getAllPlants(limit, offset);
  }

  @ApiParam({ name: "prefix", required: false, allowEmptyValue: true })
  @ApiQuery({ name: "limit", required: false })
  @ApiQuery({ name: "offset", required: false })
  @Get('/plant/search/:prefix?')
  getPlantByPrefix(@Param('prefix', new DefaultValuePipe('')) prefix: string, @Query('limit') limit: string, @Query('offset') offset: string): any {
    return this.appService.getPlantByPrefix(prefix, limit, offset);
  }

  @Get('/plant/:id')
  getPlantById(@Param('id') id: string): any {
    return this.appService.getPlantById(id);
  }

  @Get('/content')
  @ApiBearerAuth()
  getContent(@Req() req: Request): any {
    let auth_header = req.headers['authorization']?.split(' ')[1];
    if (auth_header) {
      let userID: string = Buffer.from(auth_header, 'base64').toString('ascii');
      let user_id_int: number = parseInt(userID)
      if (!user_id_int || !user_id_int) {
        return this.appService.getContent(user_id_int);
      }
      return this.appService.getContent(user_id_int);
    }
    return this.appService.getContent(0);
  }

}
