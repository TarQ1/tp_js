import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma.service';
import { AdminController } from './admin/admin.controller';
import { AdminService } from './admin/admin.service';
import { AuthMiddleware } from './admin/Auth.middleware';

@Module({
  imports: [],
  controllers: [AppController, AdminController],
  providers: [AuthMiddleware, AppService, PrismaService, AdminService],
})

export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes('admin/*');
  }
}
