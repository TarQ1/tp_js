const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()

const fs = require('fs')
const path = require('path')

const args = process.argv.slice(2)

const filePath = args[0]

const file = fs.readFileSync(path.resolve(__dirname, filePath), 'utf8')
const plants = JSON.parse(file)

const main = async () => {
    prisma.$connect()

    console.log('Deleting all plants...');
    // set all plant to status deleted
    await prisma.plants.updateMany({
        where: {
            status: true
        },
        data: {
            status: false
        }
    });

    for (const plant of plants) {
        try {
            // check if plant already exist
            const plantExist = await prisma.plants.findUnique({
                where: {
                    id: plant.id
                }
            });

            if (plantExist) {
                console.log(`Plant ${plant.id} already exist, updating with new data`);
                await prisma.plants.update({
                    where: {
                        id: plant.id
                    },
                    data: {
                        name: plant.name,
                        secondName: plant.secondName,
                        ImageUrl: plant.ImageUrl,
                        status: true
                    }
                });
            } else {
                console.log(`Plant ${plant.id} not exist, creating new plant`);
                await prisma.plants.create({
                    data: {
                        id: plant.id,
                        name: plant.name,
                        secondName: plant.secondName,
                        ImageUrl: plant.ImageUrl,
                        status: true
                    }
                });
            }
        } catch (error) {
            console.log(error)
        }
    }

}

main()
    .then(() => { console.log('plant database has been filled') })
    .catch(e => console.log(e));

