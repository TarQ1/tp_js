import { Controller, DefaultValuePipe, Get, Param, Query } from '@nestjs/common';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@Controller()
@ApiTags('Plants')
export class AppController {
  constructor(private readonly appService: AppService) { }

  @ApiQuery({ name: "limit", required: false })
  @ApiQuery({ name: "offset", required: false })
  @Get('/')
  getAllPlants(@Query('limit') limit: string, @Query('offset') offset: string): any {
    return this.appService.getAllPlants(limit, offset);
  }

  @ApiParam({ name: "prefix", required: false, allowEmptyValue: true })
  @ApiQuery({ name: "limit", required: false })
  @ApiQuery({ name: "offset", required: false })
  @Get('/plant/search/:prefix?')
  getPlantByPrefix(@Param('prefix', new DefaultValuePipe('')) prefix: string, @Query('limit') limit: string, @Query('offset') offset: string): any {
    return this.appService.getPlantByPrefix(prefix, limit, offset);
  }

  @Get('/plant/:id')
  getPlantById(@Param('id') id: string): any {
    return this.appService.getPlantById(id);
  }
}
