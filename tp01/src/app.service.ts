import { Injectable } from '@nestjs/common';
const plants = require('./../db/plant.json')

@Injectable()
export class AppService {
  getAllPlants(limit: string = '10', offset: string = '0'): any {
    let limitInt: number = parseInt(limit);
    if (!limitInt || limitInt <= 0) {
      limitInt = 10;
    }
    let offsetInt: number = parseInt(offset);
    if (!offsetInt || offsetInt <= 0) {
      offsetInt = 0;
    }
    return plants.slice(offsetInt, offsetInt + limitInt);
  }

  getPlantById(id: string): any {
    return plants.find(plant => plant.id == id);
  }

  // getPlantByPrefix
  getPlantByPrefix(prefix: string, limit: string = '10', offset: string = '0'): any {
    if (!prefix) {
      prefix = "";
    }
    let limitInt: number = parseInt(limit);
    if (!limitInt || limitInt <= 0) {
      limitInt = 10;
    }
    let offsetInt: number = parseInt(offset);
    if (!offsetInt || offsetInt <= 0) {
      offsetInt = 0;
    }
    return plants.filter(plant => plant.name.toLowerCase().startsWith(prefix.toLowerCase())).slice(offsetInt, offsetInt + limitInt);
  }
}
