# TP01 - Sylvain Ecuer

### Comment lancer l'appli : 
- Lancer le serveur avec la commande : `npm start`
- Allez sur votre navigateur à l'adresse : `http://localhost:3000/`

###  Comment lancer les tests :
- Lancer les tests avec la commande : `npm test`

### L'API :

L'api comporte trois routes :
- `GET /` : Renvoie la liste de toutes les plantes, avec limit et offset en query parameters
- `GET /plant/search/{prefix}` : Renvoie la liste des plantes dont le nom commence par le préfixe passé en paramètre avec limit et offset en query parameters
- `GET /plant/{id}` : Renvoie la plante dont l'id est passé en paramètre
  
L'endpoints `GET /plant/{id}` renvoie une structure de la forme suivante 
```
{
    "id": number
    "name": string,
    "secondName": string,
    "imageURL": string
}
```

Les autres endpoints renvoie le même type mais en tableau.
